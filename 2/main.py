from sqrt import SQRT


sqrt = SQRT()
value = input('Введите значение: ')
try:
    print(f"Корень квадратный = {sqrt(value)}")
except ValueError as e:
    print(e)
