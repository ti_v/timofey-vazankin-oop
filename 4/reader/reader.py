import os
import validators

from .file_reader import FileReader
from .url_reader import UrlReader


class Reader:

    def __call__(self, path):
        '''Чтение данных'''
        if Reader.is_url(path):
            return UrlReader.read(path)
        elif Reader.is_file_exists(path):
            return FileReader.read(path)
        else:
            raise FileNotFoundError(f'Файл или url {path} не найден')

    @classmethod
    def is_url(cls, url):
        '''Проверить путь на url'''
        return validators.url(url)

    @classmethod
    def is_file_exists(cls, path):
        '''Проверить существование файла'''
        return os.path.isfile(path)
