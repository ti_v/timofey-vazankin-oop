class FileReader:
    '''Класс для чтения данных из файла'''

    @classmethod
    def read(cls, path):
        '''Чтение данных'''
        try:
            with open(path, 'r') as f:
                return f.readline()
        except Exception:
            raise Exception(f"Произошла ошибка при чтении из файла: {path}")
