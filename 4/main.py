import argparse

from reader import Reader
from sma import SMA

from chart import Chart


parser = argparse.ArgumentParser(description='SMA_lab_4')

parser.add_argument('--paths=', help='Список адресов через запятую',
                    required=True, dest="paths")
parser.add_argument('--frame=', help='Размер окна', required=True, dest="frame")

frame = parser.parse_args().frame
paths = parser.parse_args().paths.split(',')

if len(paths) < 25:
    print('Недостаточно данных')
    exit()

if not frame.isdigit():
    print("Размер окна должен быть числом")
    exit()

frame = int(frame)

if frame < 1 or frame >= len(paths):
    print('Размер окна должен быть больше 0 и меньше кол-ва данных')
    exit()

sma = SMA()
reader = Reader()

# Чтение данных
try:
    data = [float(reader(path)) for path in paths]
except FileNotFoundError as e:
    print(e)
    exit()
except Exception as e:
    print(e)
    exit()

# нахождение sma
sma_data = sma(data, frame)

# отрисовка графика
try:
    Chart.draw(data, sma_data)
except Exception:
    print("Произошла ошибка рисования графика")
    exit()
