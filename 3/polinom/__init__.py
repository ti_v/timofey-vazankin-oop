from .polinom_manager import PolinomManager

__all__ = [
    'PolinomManager'
]
