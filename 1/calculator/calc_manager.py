from menu import Menu

from .calculator import Calculator


class CalcManager:
    '''
    Менеджер калькулятора
    '''

    def __init__(self):
        self.calc = Calculator()

    def input_value(self):
        '''
        Ввод чисел
        '''
        try:
            while True:
                if self.calc.first_value is None:
                    message = 'Введите первый операнд: '
                    property_name = 'first_value'
                else:
                    message = 'Введите второй операнд: '
                    property_name = 'second_value'

                value = input(message)
                if not value.isdigit():
                    print('Введено недопустимое значение')
                    continue

                setattr(self.calc, property_name, value)
                return value
        except KeyboardInterrupt:
            raise

    def input_command(self):
        '''
        Ввод комманды
        '''
        try:
            while True:
                command = input('Введите комманду: ')
                if not Menu.check_command(command):
                    print('Введена недопустимая команда')
                    continue
                return command
        except KeyboardInterrupt:
            raise

    def run_command(self, command):
        '''
        Выполнение команды
        '''
        try:
            res = self.calc.run_command(command)
        except ZeroDivisionError:
            print("Произошло деление на 0")
            res = None
        return res
