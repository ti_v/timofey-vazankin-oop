from calculator import CalcManager
from menu import Menu

# Основной бесконечный цикл
Menu.show()
manager = CalcManager()

try:
    x = manager.input_value()
    while True:
        y = manager.input_value()
        command = manager.input_command()
        res = manager.run_command(command)
        if not res:
            continue
        print(f"{x}{command}{y}={res}")
        x = res

except KeyboardInterrupt:
    pass
